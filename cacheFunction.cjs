function cacheFunction(callback, ...numbers) {
    let myObj = {};
    for (let value = 0; value < numbers.length; value++) {
        let index = numbers[value];
        if (myObj[index]) {
            console.log(myObj[index]);
        } else {
            inner(index);
            console.log(callback(index));
        }
        function inner(index) {
            myObj[index] = callback(index);
        }
    }
}
module.exports = cacheFunction;