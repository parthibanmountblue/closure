function mycounter(counter) {
    function increment(counter) {
        console.log(counter + 1);
    }
    function decrement(counter) {
        console.log(counter - 1);
    }
    myObj = {
        'increment': increment(counter),
        'decrement': decrement(counter)
    };
    return myObj;
}
module.exports = mycounter;