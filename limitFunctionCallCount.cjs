function limitFunctionCallCount(callback, n) {
    if (n == 0) {
        console.log(null);
    }
    else {
        return callback(n);
    }
}
module.exports = limitFunctionCallCount;